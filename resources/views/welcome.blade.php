<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Парсинг html кода</title>
        <link href="http://localhost:8080/css/app.css" rel="stylesheet">
        <!-- Styles -->
    </head>
    <body>
      <div id="app">
        <v-app>
          <v-content>
            <v-container align-content-center>
              <v-card>
                <v-card-title class="headline font-weight-regular teal lighten-3 white--text">Парсинг html кода</v-card-title>
                <v-card-text>
                  <v-textarea
                    name="input-7-1"
                    box
                    label="Введите html для парсинга"
                    auto-grow
                    value=""
                  ></v-textarea>

                  <v-btn color="success">Распарсить</v-btn>
                  <v-btn color="error">Очистить</v-btn>

                  
                </v-card-text>
              </v-card>
            </v-container>
          </v-content>
          <v-footer app>
            <v-spacer></v-spacer>
            <div>&copy; @{{ new Date().getFullYear() }}</div>
          </v-footer>
        </v-app>
      </div>
      <script src="http://localhost:8080/js/app.js"></script>
    </body>
</html>

<?php

namespace App\Classes\HtmlParser;


use App\Classes\HtmlParser\BaseNode;



/**
 *
 */
class HtmlNode extends BaseNode
{

  protected $tag;
  protected $attributes = [];

  function __construct(string $tag, string $html = '', string $inner = '')
  {
    $this->outerHtml = (strlen($html)>0)?$html:$tag;
    $this->innerHtml = $inner;
    $this->cleanTagName($tag);
  }

  protected function cleanTagName($full){
    $tag = str_replace(['<','>'], '', $full);
    if($pos = strpos($tag, ' ')){
      $tag = substr($tag, 0, $pos);
    }
    $this->tag = $tag;

    $mat = preg_match_all('/([\w-]*) *= *(([\'"])?((\\\3|[^\3])*?)\3|(\w+))/i', $full, $matches);

    if($mat){
      foreach ($matches[0] as $attr) {
        $attr = explode('=', $attr);
        $this->attributes[trim($attr[0])] = trim($attr[1], ' \t\n\r\0\x0B\'"');
      }
    }

  }



}

<?php

namespace App\Classes\HtmlParser;


use App\Classes\HtmlParser\BaseNode;



/**
 *
 */
class EmptyNode extends BaseNode
{

  function __construct($html='')
  {
    $this->outerHtml = $html;
    $this->innerHtml = $html;
  }

}

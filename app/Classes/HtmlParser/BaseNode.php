<?php

namespace App\Classes\HtmlParser;

/**
 *
 */
class BaseNode
{

  protected $outerHtml;
  protected $innerHtml;
  protected $childs = [];

  public function addChild($child){
    $this->childs[] = $child;
  }

  public function getOuterHtml(){
    return $this->outerHtml;
  }

  public function appendHtml($html, $inner = False){
    $this->outerHtml .= $html;
    if($inner){
        $this->innerHtml .= $html;
    }
  }

  public function __toString(){
    return $this->tag;
  }

}

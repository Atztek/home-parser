<?php

namespace App\Classes\HtmlParser;

use App\Classes\HtmlParser\HtmlNode;
/**
 *
 */
class HtmlParser
{
  function __construct($html)
  {
    $this->parseHtmlToNode($html);
  }

  protected function parseHtmlToNode($html){
    $parts = preg_split("/(<.*?>)/i", $html, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

    $node = $this->recursiveTags($parts);

    dd($node);
  }

  protected function recursiveTags(Array $parts, HtmlNode $current = NULL){

    if(!$current){
      $current = new EmptyNode();
    }

    for($i = 0; $i < sizeof($parts); $i++){
      $part = $parts[$i];

      if( $this->checkPart($part) == 1 ){
        $node = new HtmlNode($part);
        $current->addChild($node);
        $last = $this->recursiveTags(array_splice($parts,$i+1), $node);
        $current->appendHtml($node->getOuterHtml(), true);
        $parts = array_merge($parts, $last);
      }

      if( $this->checkPart($part) == 0 ){
        $current->appendHtml($part);
        return array_splice($parts, $i+1);
      }

      if( $this->checkPart($part) == -1 ){
        $current->appendHtml($part, true);
        $current->addChild(new EmptyNode($part));
      }
    }

    return $current;
  }

  protected function cleanTagName($tag){
    $tag = str_replace(['<','>'], '', $tag);
    if($pos = strpos($tag, ' ')){
      $tag = substr($tag, 0, $pos);
    }
    return $tag;
  }

  protected function checkPart($part){
    if(substr($part,0,2) == "</" and $part[-1] == ">"){
      return 0;
    }

    if($part[0] == "<" and $part[-1] == ">"){
      return 1;
    }

    return -1;
  }

}

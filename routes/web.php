<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Classes\HtmlParser\HtmlParser;

Route::get('/', function () {
    return view('welcome');
});



Route::get('/test', function () {
    $parser = new HtmlParser("<div><span><b class='bold' id = 'test2' >test</b></span>asdasdasd<i id='test'>test 2</i></div>");
    return '';
});
